import * as React from 'react'

function useFetch(options) {
  const {
    url,
    defaults,
    normalize = data => data,
  } = options

  const [data, setData] = React.useState(defaults)

  React.useEffect(() => {
    fetch(url).then(r => r.json()).then(data => {
      setData(normalize(data))
    })
  }, [])

  return { data }
}

export default useFetch
